package com.example.bank;

public interface Operation {
	public void execute(Account inAccount);
}
