package com.example.bank;

public class Main {
	public static void main(String[] args) {
		History hist1 = new History();
		History hist2 = new History();
		Account acc1 = new Account("1",23000,hist1);
		Account acc2 = new Account("2",5000,hist2);
		
		Bank bank = new Bank();
		
		/*bank.income(acc1, 5000);
		bank.income(acc1, 8000);*/
		
		System.out.println("Przed");
		System.out.println("acc1: " + acc1.getAmount() + " zł");
		System.out.println("acc2: " + acc2.getAmount() + " zł");
		
		bank.transfer(acc1, acc2, 3000);
		
		System.out.println("Po");
		System.out.println("acc1: " +acc1.getAmount() + " zł");
		System.out.println("acc2: " +acc2.getAmount() + " zł");
		System.out.println(hist1.showHistory());
		System.out.println(hist2.showHistory());
		
		
		
		
		
	}
}
