package com.example.bank;

import java.util.ArrayList;
import java.util.List;

public class History {
	private List<HistoryLog> logList;
	
	public History(){
		logList = new ArrayList<>();
	}

	
	public void log(HistoryLog log){
		this.logList.add(log);
	}
	
	public String showHistory(){
		StringBuilder string = new StringBuilder();
		for (HistoryLog l : this.logList)
		{
			string.append("Date of operation: ").append(l.getDateOfOperation()).append(" , \n")
			.append("Title of operation: ").append(l.getTitle()).append(" , \n")
			.append("Type of operation: ").append(l.getOperationType()).append(" \n\n");
			
		}
		return string.toString();
	}
	
}
