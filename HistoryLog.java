package com.example.bank;

import java.util.Date;

public class HistoryLog {
	private Date dateOfOperation;
	private String title;
	private OperationType operationType;
	
	



	public Date getDateOfOperation() {
		return dateOfOperation;
	}



	public void setDateOfOperation() {
		this.dateOfOperation = new Date();
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public OperationType getOperationType() {
		return operationType;
	}



	public void setOperationType(String type) {
		switch(type){
		case "Income":
			this.operationType = OperationType.income;
			break;
		case "Outcome":
			this.operationType = OperationType.outcome;
			break;
		}
	}
	



	

}
