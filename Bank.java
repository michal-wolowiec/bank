package com.example.bank;

public class Bank {
	
	public void income(Account inAccount, double inAmount){
		Operation oper = new Income(inAmount);
		inAccount.setHistory("Income");
		inAccount.doOperation(oper);
	}
	
	public void transfer(Account fromAccount, Account toAccount, double inAmount){
		Operation oper = new Transfer(fromAccount, toAccount, inAmount);
		fromAccount.setHistory("Outcome");
		toAccount.setHistory("Income");
		fromAccount.doOperation(oper);
	}
}
