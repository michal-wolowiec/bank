package com.example.bank;

import java.util.Scanner;

public class Account {
	private String number;
	private History history;
	private double amount;
	
	public Account(String number, double amount, History history){
		this.history=history;
		this.number=number;
		this.amount=amount;
	}
	
	
	public void add(double inAmount){
		this.amount+=inAmount;
	}
	
	public void subtract(double inAmount){
		this.amount-=inAmount;
	}
	
	public void doOperation(Operation op){
		op.execute(this);
	}
	
	public double getAmount(){
		return this.amount;
	}
	
	public void setHistory(String type){
		Scanner scan = new Scanner(System.in);
		HistoryLog l = new HistoryLog();
		System.out.println("Podaj tytuł transakcji: ");
		l.setTitle(scan.nextLine());
		l.setDateOfOperation();
		l.setOperationType(type);
		this.history.log(l);
	}
	
	public void showHistory(){
		history.showHistory();
	}
	
}
