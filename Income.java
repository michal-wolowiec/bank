package com.example.bank;

public class Income implements Operation{
	private Account account;
	private double amount;
	
	public Income(double amount){
		this.amount=amount;
	}
	
	@Override 
	public void execute(Account inAccount){
		inAccount.add(amount);
	}

	
}
