package com.example.bank;

public class Transfer implements Operation{
	private Account from;
	private Account to;
	private double amount;
	
	public Transfer(Account fromAccount,Account toAccount, double amount){
		this.from=fromAccount;
		this.to=toAccount;
		this.amount=amount;
	}

	@Override
	public void execute(Account account) {
		account.subtract(amount);
		to.add(amount);
		
	}
	
	
	
	
}
